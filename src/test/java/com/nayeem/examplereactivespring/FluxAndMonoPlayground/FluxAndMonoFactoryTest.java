package com.nayeem.examplereactivespring.FluxAndMonoPlayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class FluxAndMonoFactoryTest {

    List<String> names = Arrays.asList("AA","BB","CC");

    @Test
    public void fluxUsingIterable(){
        Flux<String> namesFlux = Flux.fromIterable(names);

        StepVerifier.create(namesFlux.log())
                .expectNext("AA","BB","CC")
                .verifyComplete();
    }

    @Test
    public void fluxUsingArray(){
        Flux<String> namesFlux = Flux.fromArray(new String[]{"AA","BB","CC"});

        StepVerifier.create(namesFlux.log())
                .expectNext("AA","BB","CC")
                .verifyComplete();
    }

    @Test
    public void fluxUsingStream(){
        Flux<String> namesFlux = Flux.fromStream(names.stream());

        StepVerifier.create(namesFlux.log())
                .expectNext("AA","BB","CC")
                .verifyComplete();
    }


    @Test
    public void fluxUsingRange(){
        Flux<Integer> namesFlux = Flux.range(1, 5);

        StepVerifier.create(namesFlux.log())
                .expectNext(1,2,3,4,5)
                .verifyComplete();
    }

    @Test
    public void monoUsingJustOrEmpty(){
        Mono<String> stringMono = Mono.justOrEmpty(null);

        StepVerifier.create(stringMono.log())
                .verifyComplete();
    }

    @Test
    public void monoUsingSupplier(){
        Supplier<String> stringSupplier = () -> "supplier!";

        Mono<String> stringMono = Mono.fromSupplier(stringSupplier);

        StepVerifier.create(stringMono.log())
                .expectNext("supplier!")
                .verifyComplete();
    }



}
