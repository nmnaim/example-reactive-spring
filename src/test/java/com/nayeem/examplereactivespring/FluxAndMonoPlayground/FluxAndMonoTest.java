package com.nayeem.examplereactivespring.FluxAndMonoPlayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class FluxAndMonoTest {

    /* FLUX section */

    @Test
    public void fluxTest(){
        Flux<String> stringFlux = Flux.just("A","B","C")
                .log();

        stringFlux
                .subscribe(System.out::println, (e) -> System.err.println(e));
    }

    @Test
    public void fluxExceptionTest(){
        Flux<String> stringFlux = Flux.just("AA","BB","CC")
//                .concatWith(Flux.error(new RuntimeException("Exception Test")))
                .concatWith(Flux.just("After Exception"))
                .log();

        stringFlux
                .subscribe(System.out::println,
                        (e) -> System.err.println(e)
                , () -> System.out.println("Flux Completed"));
    }

    @Test
    public void fluxTestElements_withoutError() {
        Flux<String> stringFlux = Flux.just("GTAV", "W3", "ACO")
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("GTAV")
                .expectNext("W3")
                .expectNext("ACO")
                .verifyComplete();
    }

    @Test
    public void fluxTestElements_withError() {
        Flux<String> stringFlux = Flux.just("GTAV", "W3", "ACO")
                .concatWith(Flux.error(new RuntimeException("Should have more games!!!")))
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("GTAV")
                .expectNext("W3")
                .expectNext("ACO")
//                .expectError(RuntimeException.class)
                .expectErrorMessage("Should have more games!!!")
                .verify();

    }

    @Test
    public void fluxTestElementsCount_withError() {
        Flux<String> stringFlux = Flux.just("GTAV", "W3", "ACO")
                .concatWith(Flux.error(new RuntimeException("Should have more games!!!")))
                .log();

        StepVerifier.create(stringFlux)
                .expectNextCount(3)
//                .expectError(RuntimeException.class)
                .expectErrorMessage("Should have more games!!!")
                .verify();

    }

    /* FLUX section */

    /* MONO section */

    @Test
    public void monoTest(){
        Mono<String> stringMono = Mono.just("TEST")
                .log();

        StepVerifier.create(stringMono)
                .expectNext("TEST")
                .verifyComplete();
    }

    @Test
    public void monoTest_withError(){
        StepVerifier.create(Mono.error(new RuntimeException("MONO ERROR!!!!")).log())
                .expectErrorMessage("MONO ERROR!!!!")
                .verify();
    }

    /* MONO section */
}
