package com.nayeem.examplereactivespring.FluxAndMonoPlayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

public class FluxAndMonoFilterTest {

    List<String> names = Arrays.asList("Alim","Aziz","Badol","Bazlur","Zahid","Xujayed");

    @Test
    public void filterTest_prefix(){
        Flux<String> namesFLux = Flux.fromIterable(names)
                .filter(elem -> elem.startsWith("A"))
                .log();

        StepVerifier.create(namesFLux)
                .expectNext("Alim","Aziz")
                .verifyComplete();


    }

    @Test
    public void filterTest_length(){
        Flux<String> namesFLux = Flux.fromIterable(names)
                .filter(elem -> elem.length() == 5)
                .log();

        StepVerifier.create(namesFLux)
                .expectNext("Badol","Zahid")
                .verifyComplete();


    }
}
