package com.nayeem.examplereactivespring.FluxAndMonoPlayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class FluxAndMonoErrorTest {

    @Test
    public void fluxErrorHandling(){
        Flux<String> stringFlux = Flux.just("A","B","C")
                .concatWith(Flux.error(new RuntimeException("EXCEPTION")))
                .concatWith(Flux.just("D"))
                .onErrorResume(e -> {
                    System.err.println("Exception is : "+e);
                    return Flux.just("default");
                });

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("A","B","C")
//                .expectError()
                .expectNext("default")
                .verifyComplete();
    }

    @Test
    public void fluxErrorHandling_onErrorReturn(){
        Flux<String> stringFlux = Flux.just("A","B","C")
                .concatWith(Flux.error(new RuntimeException("EXCEPTION")))
                .concatWith(Flux.just("D"))
                .onErrorReturn("default");

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("A","B","C")
//                .expectError()
                .expectNext("default")
                .verifyComplete();
    }

    @Test
    public void fluxErrorHandling_onErrorMap(){
        Flux<String> stringFlux = Flux.just("A","B","C")
                .concatWith(Flux.error(new RuntimeException("EXCEPTION")))
                .concatWith(Flux.just("D"))
                .onErrorMap(e -> new CustomException(e));

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("A","B","C")
//                .expectError()
                .expectError(CustomException.class)
                .verify();
    }

    @Test
    public void fluxErrorHandling_onErrorMap_withRetry(){
        Flux<String> stringFlux = Flux.just("A","B","C").delaySubscription(Duration.ofSeconds(1))
                .concatWith(Flux.error(new RuntimeException("EXCEPTION")))
                .concatWith(Flux.just("D"))
                .onErrorMap(e -> new CustomException(e))
                .retry(2);

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("A","B","C")
                .expectNext("A","B","C")
                .expectNext("A","B","C")
//                .expectError()
                .expectError(CustomException.class)
                .verify();
    }


    @Test
    public void fluxErrorHandling_onErrorMap_withRetryBackoff(){
        Flux<String> stringFlux = Flux.just("A","B","C").delaySubscription(Duration.ofSeconds(1))
                .concatWith(Flux.error(new RuntimeException("EXCEPTION")))
                .concatWith(Flux.just("D"))
                .onErrorMap(e -> new CustomException(e))
                .retryBackoff(2, Duration.ofSeconds(5)); // use retryWhen

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("A","B","C")
                .expectNext("A","B","C")
                .expectNext("A","B","C")
//                .expectError()
                .expectError(CustomException.class)
                .verify();
    }

    private class CustomException extends Throwable{

        private String msg;

        public CustomException(Throwable e) {
            this.msg = e.getMessage();
        }

        public String getMsg() {
            return msg;
        }
    }
}
