package com.nayeem.examplereactivespring.FluxAndMonoPlayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FluxAndMonoTransfromTest {

    List<String> names = Arrays.asList("Alim", "Aziz", "Xujayed");

    @Test
    public void transformUsingMap() {
        Flux<String> namesFlux = Flux.fromIterable(names)
                .map(name -> name.toLowerCase().substring(0, 2))
                .log();

        StepVerifier.create(namesFlux)
                .expectNext("al", "az", "xu")
                .verifyComplete();
    }

    @Test
    public void transformUsingMap_repeat() {
        Flux<String> namesFlux = Flux.fromIterable(names)
                .filter(name -> name.startsWith("A"))
                .map(name -> name.toLowerCase().substring(0, 2))
                .repeat(1)
                .log();

        StepVerifier.create(namesFlux)
                .expectNext("al", "az", "al", "az")
                .verifyComplete();
    }

    @Test
    public void transformUsingFlatMap() {
        Flux<String> stringFlux = Flux.fromIterable(List.of("A","B","C","D","E","F"))
                .flatMap( s -> {
                    return Flux.fromIterable(convertToLIst(s)); // A -> List[A, newValue]  B -> List[B, newValue]
                }) // db call or external service call that returns flux;  s -> Flux<String>
                .log();

        StepVerifier.create(stringFlux)
                .expectNextCount(12)
                .verifyComplete();
    }

    @Test
    public void transformUsingFlatMap_usingParallel() {
        Flux<String> stringFlux = Flux.fromIterable(List.of("A","B","C","D","E","F"))
                .window(2) // Flux<Flux<String>> -> (A,B),  (C,D),  (E,F)
                .flatMap( s -> s.map(this::convertToLIst).subscribeOn(Schedulers.parallel())) // parallel call which return Flux<List<String>>
                .flatMap(elem -> Flux.fromIterable(elem)) // FLux<String>
                .log();

        StepVerifier.create(stringFlux)
                .expectNextCount(12)
                .verifyComplete();
    }

    @Test
    public void transformUsingFlatMap_usingParallel_maintain_order() {
        Flux<String> stringFlux = Flux.fromIterable(List.of("A","B","C","D","E","F"))
                .window(2) // Flux<Flux<String>> -> (A,B),  (C,D),  (E,F)
//                .concatMap( s -> s.map(this::convertToLIst).subscribeOn(Schedulers.parallel())) // parallel call which return Flux<List<String>>
                .flatMapSequential( s -> s.map(this::convertToLIst).subscribeOn(Schedulers.parallel())) // parallel call which return Flux<List<String>>
                .flatMap(elem -> Flux.fromIterable(elem)) // FLux<String>
                .log();

        StepVerifier.create(stringFlux)
                .expectNextCount(12)
                .verifyComplete();
    }

    private List<String> convertToLIst(String s) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Arrays.asList(s, "newValue");
    }
}
