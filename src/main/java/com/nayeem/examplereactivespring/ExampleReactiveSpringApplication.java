package com.nayeem.examplereactivespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleReactiveSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleReactiveSpringApplication.class, args);
	}

}
