package com.nayeem.examplereactivespring.router;

import com.nayeem.examplereactivespring.service.ProductService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Component
public class ProductRouter {
    @Bean
    public RouterFunction route(ProductService productService) {
        return RouterFunctions
                .route(POST("/products").and(contentType(APPLICATION_JSON)), productService::createProduct)
                .andRoute(GET("/products").and(RequestPredicates.accept(APPLICATION_JSON)), productService::findAllProducts)
                .andRoute(GET("/products/{id}").and(RequestPredicates.accept(APPLICATION_JSON)), productService::findProductById)
                .andRoute(PUT("/products/{id}").and(accept(APPLICATION_JSON)), productService::updateProduct)
                .andRoute(DELETE("/products/{id}").and(accept(APPLICATION_JSON)), productService::deleteProduct);
    }
}
