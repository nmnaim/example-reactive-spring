package com.nayeem.examplereactivespring.repository;

import com.nayeem.examplereactivespring.model.Product;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductRepository extends ReactiveMongoRepository<Product, String> {
}