package com.nayeem.examplereactivespring.service;

import com.nayeem.examplereactivespring.model.Product;
import com.nayeem.examplereactivespring.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.*;

@Component
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public Mono<ServerResponse> findAllProducts(ServerRequest request) {

//        Flux<Product> productFlux = Flux.just(new Product("1","testName1","testCategory1",110d)
//                , new Product("2","testName2","testCategory2",220d)
//                , new Product("3","testName3","testCategory3",333d));

        return ServerResponse.ok()
                .contentType(APPLICATION_STREAM_JSON)
//                .body(productFlux, Product.class);
                .body(productRepository.findAll(), Product.class);
    }

    public Mono<ServerResponse> findProductById(ServerRequest request) {

//        Mono<Product> productMono = Mono.just(new Product("1","testName","testCategory",20.25));

        return ServerResponse.ok()
                .contentType(APPLICATION_STREAM_JSON)
//                .body(productMono, Product.class);
                .body(productRepository.findById(request.pathVariable("id")), Product.class);
    }

    public Mono<ServerResponse> createProduct(ServerRequest request) {
        Mono<Product> productMono = request.bodyToMono(Product.class)
                .flatMap(product -> productRepository.save(product));
        return ServerResponse.ok()
                .contentType(APPLICATION_STREAM_JSON)
                .body(productMono, Product.class);
    }

    public Mono<ServerResponse> updateProduct(ServerRequest request) {
        Mono<Product> productMono = request.bodyToMono(Product.class)
                .flatMap(product ->
                        productRepository.findById(request.pathVariable("id"))
                                .map(e -> {
                                    e.setName(product.getName());
                                    e.setCategory(product.getCategory());
                                    e.setPrice(product.getPrice());
                                    return e;
                                }).flatMap(elem -> productRepository.save(elem)));
        return ServerResponse.ok()
                .contentType(APPLICATION_STREAM_JSON)
                .body(productMono, Product.class);
    }

    public Mono<ServerResponse> deleteProduct(ServerRequest request) {
        return ServerResponse.ok()
                .contentType(APPLICATION_STREAM_JSON)
                .body(productRepository.deleteById(request.pathVariable("id")), Product.class);
    }
}