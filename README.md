# Getting Started
The following was endpoints are discoverable:

- GET localhost:8080/products
- GET localhost:8080/products/{id}
- POST localhost:8080/products -> `{"id":"1","name":"testName1","category":"testCategory1","price":100}`
- PUT localhost:8080/products/{id} -> `{"name":"testName2","category":"testCategory2","price":200}`
- DELETE localhost:8080/products/{id}

__NB.__ THe embedded mongo DB only works in linux

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/#build-image)
* [Spring Data Reactive MongoDB](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#boot-features-mongodb)

